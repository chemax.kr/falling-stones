/****
 *
 * TODO: animations of dead
 *
 * TODO: new background
 *
 * TODO: lifetime for stars
 *
 * TODO: mobile platform
 *
 * TODO: safe zone for player
 *
 * TODO: time of game
 *
 * TODO: заменяем всплывающий текст на спрайты, каждый спрайт как объект со своим временем жизни.
 * можно попробовать прозрачные спрайты к которым привязывать текст. Или вообще объекты без спрайтов.
 * Пример посмотреть тут: http://phaser.io/examples/v2/input/virtual-gamecontroller
 *
 * TODO: Виртуальный контроллер по кнопке вверху справа. Пример реализации тут:
 * http://phaser.io/examples/v2/input/virtual-gamecontroller
 *
 * TODO: сделать gamestages. Меню. 
 *
 */


var game = new Phaser.Game(window.innerWidth * window.devicePixelRatio * 0.9, window.innerHeight * window.devicePixelRatio * 0.9, Phaser.AUTO, 'Falling Stones.', this);

function preload() {

    game.load.image('star', 'img/sprites/star.png');
    game.load.image('star_blue', 'img/sprites/star2.png');
    game.load.image('star_green', 'img/sprites/star3.png');
    game.load.image('star_red', 'img/sprites/star4.png');
    game.load.spritesheet('rolling_stones', 'img/sprites/rolling_stones.png', 48, 48)
    game.load.spritesheet('player', 'img/sprites/crowley_wings.png', 48, 48);


}

var nickname = "Some angel";
var stars;
var rats;

var plScore = 0;
var star;
var stateText;
var starText;
var stoneText;
var topScorePlayer = 0;
var topScoreAll = 0 ;
var playerSpeedNormal = 200;
var playerSpeed = 200;
var starScore = 3;
var starType = 1;
var speedMod;
var upKey;
var downKey;
var leftKey;
var rightKey;
var scoreTimer;
var deadScoreTimer;
var playerTopScoreText;
var playerAllTopScoreText;
var playerScoreText;
var playerSpeedText;
var leftMobile = false;
var rightMobile = false;
var upMobile = false;
var downMobile =false;



function create() {

    WASDKeyAdd();

    this.game.canvas.id = 'canvas_1';

    game.physics.startSystem(Phaser.Physics.ARCADE);
    game.physics.arcade.setBoundsToWorld();

    game.stage.backgroundColor = '#6688ee';

    playerTopScoreText = game.add.text(5, 10, 'Your top-score: ' + plScore, { fill: '#aaffaa', font: '14pt Arial' });
    playerAllTopScoreText = game.add.text(5, 30, 'Top score: ' + plScore, { fill: '#aaffaa', font: '14pt Arial' });
    playerScoreText = game.add.text(5, 50, 'Score: ' + plScore, { fill: '#aaffaa', font: '14pt Arial' });
    playerSpeedText = game.add.text(5, 65, 'Speed: ' + playerSpeed, { fill: '#aaffaa', font: '14pt Arial' });

    rats = game.add.physicsGroup();

    stars = game.add.group();

    star = stars.create(game.world.randomX+40, game.world.randomY, 'star');
    var y = -600;

    ratsCount = Math.round(window.innerHeight * window.devicePixelRatio * 0.9 / 30);
    console.log("Rats: " + ratsCount);
    for (var i = 0; i < ratsCount; i++)
    {

        var rat = rats.create(game.world.randomX+40, y, 'rolling_stones');
        rat.body.setCircle(24);
        rat.animations.add('rolling_down', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22]);
        rat.body.velocity.y = game.rnd.between(100, 300);
        y += 48;
    }

    player = game.add.sprite(20, 300, 'player');


    player.animations.add("player_up", [12,13,14,15]);
    player.animations.add("player_down", [0,1,2,3]);
    player.animations.add("player_right", [4,5,6,7]);
    player.animations.add("player_left", [8,9,10,11]);


    player.anchor.set(0.5);

    game.physics.arcade.enable(player);
    game.physics.arcade.enable(stars);
    player.body.collideWorldBounds = true;

    cursors = game.input.keyboard.createCursorKeys();

    stateText = game.add.text(game.world.centerX,game.world.centerY,' ', { font: '24px Arial', fill: '#fff' });
    stateText.anchor.setTo(0.5, 0.5);
    stateText.visible = false;

    starText = game.add.text(game.world.centerX,game.world.centerY,' ', { font: '18px Arial', fill: '#47fa00' });
    starText.anchor.setTo(0.5, 0.5);
    starText.visible = false;

    stoneText = game.add.text(game.world.centerX,game.world.centerY,' ', { font: '18px Arial', fill: '#47fa00' });
    stoneText.anchor.setTo(0.5, 0.5);
    stoneText.visible = false;
    game.physics.arcade.enable([ stoneText, starText]);
    player.body.setSize(28, 48, 10);
}

function WASDKeyAdd() {
    upKey = game.input.keyboard.addKey(Phaser.Keyboard.W);
    downKey = game.input.keyboard.addKey(Phaser.Keyboard.S);
    leftKey = game.input.keyboard.addKey(Phaser.Keyboard.A);
    rightKey = game.input.keyboard.addKey(Phaser.Keyboard.D);
}


function update() {


    rats.forEach(checkPos, this);
    game.physics.arcade.overlap(player, rats, collisionHandler, null, this);
    game.physics.arcade.overlap(player, stars, collectStar, null, this);
    playerSpeedText.text = "Speed: " + playerSpeed;
    player.body.velocity.x = 0;
    player.body.velocity.y = 0;
    


    if( (cursors.left.isDown) || (leftKey.isDown) || leftMobile)
    {
        player.body.velocity.x = -playerSpeed;
        player.animations.play("player_left");
    }
    else if ((cursors.right.isDown) || (rightKey.isDown) || rightMobile)
    {
        player.body.velocity.x = playerSpeed;
        player.animations.play("player_right");
    }

    if ((cursors.up.isDown) || (upKey.isDown) || upMobile)
    {
        player.body.velocity.y = -playerSpeed;
        player.animations.play("player_up");
    }
    else if ((cursors.down.isDown) || (downKey.isDown) || downMobile)
    {
        player.body.velocity.y = playerSpeed;
        player.animations.play("player_down");
    }

}

function render() {

    game.debug.text('Elapsed seconds: ' + this.game.time.totalElapsedSeconds(), 32, 32);

}

function checkPos (rat) {

    rat.animations.currentAnim.speed = rat.body.velocity.y/5;
    rat.animations.play('rolling_down');

    if (rat.y > 800)
    {
        ratRespawn(rat);

    }

}

function collisionHandler (player, rat) {

    minusScore = Math.round(topScorePlayer*0.15+1);
    plScore-=minusScore;
    stoneText.text = "-" + minusScore + " score!";
    if (deadScoreTimer)
    {
        deadScoreTimer.timer.remove(deadScoreTimer.event);

    }

    deadScoreTimer = game.time.events.add(Phaser.Timer.SECOND * 4, function () {stoneText.visible = false;}, this);

    stoneText.x = player.x;
    stoneText.y = player.y;
    stoneText.visible = true;
    stoneText.body.velocity.y = -20;
    player.x = 20;
    player.y = 300;

    if (plScore < 0)
    {
        stateText.text=" GAME OVER \n" + playerTopScoreText.text + "\n Click to restart";
        nickname = prompt("Enter your nickname", nickname);
        scoreUpdate(nickname, topScorePlayer);
        stateText.visible = true;
        player.kill();
        game.input.onTap.addOnce(restart,this);

    }

    playerScoreText.text = 'Score: ' + plScore;
    ratRespawn(rat);

}

function collectStar (player, star) {

    starType = Math.round(selectStarType());
    console.log(starType);

    starText.text = starScore + " score!";

    starText.x = star.x;
    starText.y = star.y;
    starText.visible = true;
    starText.body.velocity.y = -20;

    if (scoreTimer)
    {
        scoreTimer.timer.remove(scoreTimer.event);

    }

    scoreTimer = game.time.events.add(Phaser.Timer.SECOND * 4, function () {starText.visible = false;}, this);
/****
 * TODO: startypes (bonuses and anti-bonuses):
 * 1) simple star, 3 score, without any effects
 * 2) green speed star, 5 score, 10% bonus to speed, 10 second
 * 3) blue star, 5 score, invulnerability, 5 second
 * 4) red star, any of the above: -5 score, -20% speed for 10 second.
 * */


    switch (starType){
        case 0: //blue star
            starScore = 5;
            playerSpeed = Math.round(playerSpeedNormal*2);
            console.log (playerSpeed);
            star.loadTexture("star_blue", 0);
            console.log(starType + " star_blue");
            speedMod = game.time.events.add(Phaser.Timer.SECOND * 2, function() {playerSpeed = playerSpeedNormal;}, this);

            break;
        case 1: //green star
            starScore = 5;
            star.loadTexture("star_green", 0);
            console.log(starType + " star_green");
            //star.sprite.set("star_green");
            break;
        case 2: //red start
            starScore = 1;
            playerSpeed = Math.round(playerSpeedNormal*0.6);
            console.log (playerSpeed);
            console.log(starType + " star_red");
            game.time.events.add(Phaser.Timer.SECOND * 2, function() {playerSpeed = playerSpeedNormal;}, this);
            break;
        default: //simple star
            starScore = 3;
            star.loadTexture("star", 0);
            playerSpeed = playerSpeedNormal;

    }

    star.x=game.rnd.between(50, 750);
    star.y=game.rnd.between(50, 550);

    plScore+=starScore;

    if (topScorePlayer < plScore)
    {
        topScorePlayer = plScore;
        playerTopScoreText.text = 'Your top-score: ' + topScorePlayer;
    }
    if (topScoreAll < topScorePlayer)
    {
        topScoreAll = topScorePlayer;
        playerAllTopScoreText.text = 'Top score: ' + topScoreAll;
    }

    playerScoreText.text = 'Score: ' + plScore;


}

function selectStarType() {

    return getRandom(0,4);
}

function getRandom(min, max){
    return Math.random() * (max - min) + min;
}

function restart () {

    //scoreUpdate(topScorePlayer);
    player.revive();
    starScore = 3;
    star.loadTexture("star", 0);
    playerSpeed = playerSpeedNormal;
    //hides the text
    stateText.visible = false;
    stoneText.visible = false;
    starText.visible = false;
    plScore = 0;
    playerScoreText.text = 'Score: ' + plScore;
    topScorePlayer = plScore;
    playerTopScoreText.text = 'Your top-score: ' + topScorePlayer;

}

function ratRespawn (rat) {
    rat.x=game.world.randomX+60;
    rat.y = -100;
    rat.body.velocity.y = game.rnd.between(100, 300);
}

function scoreUpdate (nickname, topscore) {
    //File data test
    console.log(topscore);
    var data = new FormData();

    var xhr = new XMLHttpRequest();

    xhr.open( 'get', 'score.php?nickname='+nickname+'&score='+topscore+'&gametime='+this.game.time.totalElapsedSeconds(), true );
    xhr.send();

}



// var myElement = document.getElementById('gamebody');
//
// // create a simple instance
// // by default, it only adds horizontal recognizers
// var mc = new Hammer(myElement);
//
// // let the pan gesture support all directions.
// // this will block the vertical scrolling on a touch-device while on the element
// mc.get('pan').set({ direction: Hammer.DIRECTION_ALL });
//
// // listen to events...
// mc.on("panleft panright panup pandown tap press", function(ev) {
//     myElement.textContent = ev.type +" gesture detected.";
// });